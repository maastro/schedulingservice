# MIA Scheduling Service #

The SchedulingService schedules queued containers and relegates them to a worker capable of performing their computations. This enables the user to use multiple worker nodes within the MIA project to scale for performance. It also enables the usage of different workers for different types of computations.

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser