package nl.maastro.mia.schedulingservice;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.schedulingservice.SchedulingServiceApplication;
import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.service.TaskService;
import nl.maastro.mia.schedulingservice.web.dto.TaskDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SchedulingServiceApplication.class)
public class PrioritySchedulingTests {
    
    @Autowired
    TaskService taskService;
    
	@Test
	public void testEqualPriority() {
		TaskDto task1a100 = new TaskDto();
		task1a100.setContainerId("1");
		task1a100.setCalculationService("a");
		task1a100.setPriority(100);
		taskService.addTask(task1a100);
		
		TaskDto task2a100 = new TaskDto();
        task2a100.setContainerId("2");
        task2a100.setCalculationService("a");
        task2a100.setPriority(100);
        taskService.addTask(task2a100);
        
        ScheduledTask task = taskService.getNext("a");
        assertEquals("1", task.getContainerId());
        
        taskService.getTasks().stream().forEach(t -> taskService.deleteTask(t.getId()));
	}
	
	@Test
    public void testHigherPriority() {
	    TaskDto task1a100 = new TaskDto();
        task1a100.setContainerId("1");
        task1a100.setCalculationService("a");
        task1a100.setPriority(100);
        taskService.addTask(task1a100);
        
        TaskDto task2a200 = new TaskDto();
        task2a200.setContainerId("2");
        task2a200.setCalculationService("a");
        task2a200.setPriority(200);
        taskService.addTask(task2a200);
        
        ScheduledTask task = taskService.getNext("a");
        assertEquals("2", task.getContainerId());
        
        taskService.getTasks().stream().forEach(t -> taskService.deleteTask(t.getId()));
	}
	
	@Test
    public void testPriorityCalcuationServiceMix() {
	    TaskDto task1a100 = new TaskDto();
	    task1a100.setContainerId("1");
	    task1a100.setCalculationService("a");
	    task1a100.setPriority(100);
	    taskService.addTask(task1a100);
	    
	    TaskDto task2a500 = new TaskDto();
        task2a500.setContainerId("2");
        task2a500.setCalculationService("a");
        task2a500.setPriority(500);
        taskService.addTask(task2a500);
        
        TaskDto task3a100 = new TaskDto();
        task3a100.setContainerId("3");
        task3a100.setCalculationService("a");
        task3a100.setPriority(100);
        taskService.addTask(task3a100);
        
        TaskDto task4b100 = new TaskDto();
        task4b100.setContainerId("4");
        task4b100.setCalculationService("b");
        task4b100.setPriority(100);
        taskService.addTask(task4b100);
        
        TaskDto task5b300 = new TaskDto();
        task5b300.setContainerId("5");
        task5b300.setCalculationService("b");
        task5b300.setPriority(300);
        taskService.addTask(task5b300);
        
        ScheduledTask t1 = taskService.getNext("b");
        assertEquals("5", t1.getContainerId());
        taskService.deleteTask(t1.getId());
        
        ScheduledTask t2 = taskService.getNext("a");
        assertEquals("2", t2.getContainerId());
        taskService.deleteTask(t2.getId());
        
        ScheduledTask t3 = taskService.getNext("b");
        assertEquals("4", t3.getContainerId());
        taskService.deleteTask(t3.getId());
        
        ScheduledTask t4 = taskService.getNext("a");
        assertEquals("1", t4.getContainerId());
        taskService.deleteTask(t4.getId());
        
        ScheduledTask t5 = taskService.getNext("a");
        assertEquals("3", t5.getContainerId());
        taskService.deleteTask(t5.getId());
	}

}
