package nl.maastro.mia.schedulingservice.web.controller;

import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;
import nl.maastro.mia.schedulingservice.service.ContainerService;

@RestController
@RequestMapping("/api")
public class CalculationController {

	private ContainerService containerService;
	private TaskRepository taskRepository;

	public CalculationController(ContainerService containerService, TaskRepository taskRepository) {
		this.containerService = containerService;
		this.taskRepository = taskRepository;
	}

	@RequestMapping(value="/computation/{id}", method=RequestMethod.GET)
	public ResponseEntity<Boolean> updateCalculation(@PathVariable String id, @RequestParam TaskStatus taskStatus){
		ScheduledTask task = taskRepository.findOneByCalculationId(id);
		if(task==null)
			return ResponseEntity.notFound().build();

		containerService.updateCalculationStatus(task, taskStatus);
		return ResponseEntity.ok(Boolean.TRUE);
	}
	
}
