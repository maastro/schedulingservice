package nl.maastro.mia.schedulingservice.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.schedulingservice.entity.CalculationService;
import nl.maastro.mia.schedulingservice.repository.CalculationServiceRepository;

@RestController
@RequestMapping("/api")
public class CalculationServiceController {
	
	@Autowired
	CalculationServiceRepository calculationServiceRepository;
	
	@RequestMapping(value="/calculationservice", method=RequestMethod.GET)
	public List<CalculationService> getCalculationServiceList(){
		return calculationServiceRepository.findAll();
	}

}
