package nl.maastro.mia.schedulingservice.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.service.TaskService;
import nl.maastro.mia.schedulingservice.web.dto.TaskDto;

@RestController
@RequestMapping("/api")
public class TaskController {
	
	@Autowired
	TaskService taskService;
	
	@RequestMapping(value="/task/{id}", method=RequestMethod.GET)
	public ScheduledTask getTask(@PathVariable Long id){
		return taskService.getTask(id);
	}
	
	@RequestMapping(value="/task/{id}", method=RequestMethod.DELETE)
	public boolean deleteTask(@PathVariable Long id){
		return taskService.deleteTask(id);
	}
	
	@RequestMapping(value="/task", method=RequestMethod.GET)
	public List<ScheduledTask> getTaskList(
			@RequestParam(value="calculationservice", required=false, defaultValue="") String calculationService){
		if(calculationService.isEmpty())
			return taskService.getTasks();
		else
			return taskService.findTasksByCalculationService(calculationService);		
	}
	
	@RequestMapping(value="/task", method=RequestMethod.POST)
	public ScheduledTask postTask(@RequestBody TaskDto taskDto){
		return taskService.addTask(taskDto);
	}
	
	@RequestMapping(value="/task/next", method=RequestMethod.GET)
	public ScheduledTask getTaskNext(@RequestParam String calculationService){
		return taskService.getNext(calculationService);
	}

	@RequestMapping(value="/task/setpriority", method=RequestMethod.GET)
	public ResponseEntity<List<ScheduledTask>> setPriority(@RequestParam String containerid, @RequestParam int priority) {
		List<ScheduledTask> outTasks = taskService.setPriority(containerid, priority);

		if (outTasks.isEmpty())
			return ResponseEntity.notFound().build();
		else
			return ResponseEntity.ok(outTasks);
	}

	@RequestMapping(value="/task/getpriority", method=RequestMethod.GET)
	public ResponseEntity<Integer> getPriority(@RequestParam String containerid) {
		Integer priority = taskService.getPriority(containerid);

		if(priority==null)
			return ResponseEntity.notFound().build();
		else
			return ResponseEntity.ok(priority);
	}
}
