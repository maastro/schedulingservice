package nl.maastro.mia.schedulingservice;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SchedulingServiceApplication {

	private static final Logger LOGGER = Logger.getLogger(SchedulingServiceApplication.class);
	private static final String name = SchedulingServiceApplication.class.getPackage().getName();
	private static final String version = SchedulingServiceApplication.class.getPackage().getImplementationVersion();
	
	public static void main(String[] args) {    	
		SpringApplication.run(SchedulingServiceApplication.class, args);
		LOGGER.info("**** VERSION **** : " + name + " " +  version );  
	}
}
