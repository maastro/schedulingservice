package nl.maastro.mia.schedulingservice.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;

@Entity     
public class ScheduledTask {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	 
    @ManyToOne
	CalculationService calculationService = null;
	    	
    @NotNull
	private String containerId;
    
    private String calculationId;
    
    private String instanceId;
    
    private TaskStatus taskStatus;
    				
	private String userId = null;
	
	int priority = 100;
	
	Date dateTask = new Date();
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CalculationService getCalculationService() {
		return calculationService;
	}

	public void setCalculationService(CalculationService calculationService) {
		this.calculationService = calculationService;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Date getDateTask() {
		return dateTask;
	}

	public void setDateTask(Date dateTask) {
		this.dateTask = dateTask;
	}

	public String getCalculationId() {
		return calculationId;
	}

	public void setCalculationId(String calculationId) {
		this.calculationId = calculationId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public TaskStatus getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatus taskStatus) {
		this.taskStatus = taskStatus;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", calculationService=" + calculationService + ", containerId=" + containerId
				+ ", calculationId=" + calculationId + ", instanceId=" + instanceId + ", taskStatus=" + taskStatus
				+", userId=" + userId + ", priority=" + priority
				+ ", dateTask=" + dateTask + "]";
	}
		
}