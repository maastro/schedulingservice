package nl.maastro.mia.schedulingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.schedulingservice.entity.CalculationService;

@Repository
public interface CalculationServiceRepository extends JpaRepository<CalculationService, Long>{
	
	CalculationService findOneByName(String name);

}
