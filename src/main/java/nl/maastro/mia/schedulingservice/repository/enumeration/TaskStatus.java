package nl.maastro.mia.schedulingservice.repository.enumeration;

/**
 * The CalculationStatus enumeration.
 */
public enum TaskStatus {
	
    QUEUE, RUNNING, DONE, CALCULATIONERROR, EXPORTERROR, CONNECTIONERROR, REQUEUE, RERUNNING, IDLE
    
}
