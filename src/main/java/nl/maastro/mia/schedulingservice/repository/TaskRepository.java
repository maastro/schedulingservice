package nl.maastro.mia.schedulingservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.schedulingservice.entity.CalculationService;
import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;

@Repository
public interface TaskRepository extends JpaRepository<ScheduledTask, Long>{

	ScheduledTask findTop1ByTaskStatusAndCalculationServiceOrderByPriorityDesc(TaskStatus taskStatus, CalculationService calculationService);
	
	ScheduledTask findOneByCalculationId(String calculationId);
	
	List<ScheduledTask> findByCalculationService(CalculationService calculationService);
	
	List<ScheduledTask> findByTaskStatus(TaskStatus taskStatus);
	
	public long countByCalculationService(CalculationService calculationService);
	
	public long countByTaskStatus(TaskStatus taskStatus);

	List<ScheduledTask> findByContainerId(String containerId);
}
