package nl.maastro.mia.schedulingservice.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import nl.maastro.mia.schedulingservice.service.TaskSchedulerService;

@Configuration
@EnableScheduling
public class SchedulingConfig {
	
	@Autowired
	TaskSchedulerService taskQueueService;
	
	private final static Logger LOGGER = Logger.getLogger(SchedulingConfig.class);
	
	private static boolean busySubmitting = false;
	private static boolean busyChecking = false;

	// Check for available workers and submit tasks from queue
    @Scheduled(fixedRateString = "${scheduler.queuedTasksCheck:10000}")
    private void submit() {
    	try {
    		if (!busySubmitting) {
        		busySubmitting = true;
        		LOGGER.info("scheduler.queuedTasksCheck started");
        		taskQueueService.checkQueuedTasks();
        		busySubmitting = false;
        		LOGGER.info("scheduler.queuedTasksCheck finished");
        	}
        	else {
        		LOGGER.info("scheduler.queuedTasksCheck still busy...");
        	}
    	}
    	catch (Exception e) {
    		busySubmitting = false;
    		LOGGER.error("scheduler.queuedTasksCheck error while checking", e);
    	}
    }
    
    // Check that active workers are still reachable
    @Scheduled(fixedRateString = "${scheduler.runningTasksCheck:60000}")
    private void checkHeartbeat() {
    	try {
    		if(!busyChecking){
        		busyChecking = true;
        		LOGGER.info("scheduler.runningTasksCheck started");
        		taskQueueService.checkRunningTasks();
        		busyChecking = false;
            	LOGGER.info("scheduler.runningTasksCheck finished");
        	}
        	else {
        		LOGGER.info("scheduler.runningTasksCheck still busy...");
        	}
    	} 
    	catch (Exception e) {
    		busyChecking = false;
        	LOGGER.info("scheduler.runningTasksCheck error while checking", e);
    	}
    }
    
}