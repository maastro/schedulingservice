package nl.maastro.mia.schedulingservice.service;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import nl.maastro.mia.schedulingservice.entity.CalculationService;
import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.repository.CalculationServiceRepository;
import nl.maastro.mia.schedulingservice.repository.TaskRepository;
import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;
import nl.maastro.mia.schedulingservice.web.dto.TaskDto;

@Service
public class TaskService {
	
	private final static Logger LOGGER = Logger.getLogger(TaskService.class);
	
	@Autowired
	TaskRepository taskRepository;
	
	@Autowired
	CalculationServiceRepository calculationServiceRepository;
	
	@Value("${task.enablepriority:true}")
	private boolean priorityEnabled;
	
	
	public List<ScheduledTask> getTasks(){
		return taskRepository.findAll();
	}

	public ScheduledTask addTask(TaskDto dto){

		CalculationService calculationService = calculationServiceRepository.findOneByName(dto.getCalculationService());
		if(calculationService==null){
			calculationService = new CalculationService();
			calculationService.setName(dto.getCalculationService());
			calculationServiceRepository.save(calculationService);
		}
		ScheduledTask task = mapTask(dto);
		task.setCalculationService(calculationService);
		
		if(priorityEnabled){
			task.setPriority(dto.getPriority());
		}
		LOGGER.debug("Save task:"+task);
		return taskRepository.save(task);
	}

	public ScheduledTask getTask(Long id) {
		return taskRepository.findOne(id);
	}

	public boolean deleteTask(Long id) {
		ScheduledTask task = taskRepository.findOne(id);
		if(task==null){
			return false;
		}
		else{
			taskRepository.delete(id);
			return true;
		}
	}
	
	
	public ScheduledTask getNext(String calculatonServiceName) {
		CalculationService calculationService = calculationServiceRepository.findOneByName(calculatonServiceName);
		if(calculationService==null)
			return null;

		ScheduledTask queueTask = taskRepository.findTop1ByTaskStatusAndCalculationServiceOrderByPriorityDesc(TaskStatus.QUEUE, calculationService);
		ScheduledTask requeueTask = taskRepository.findTop1ByTaskStatusAndCalculationServiceOrderByPriorityDesc(TaskStatus.REQUEUE, calculationService);

		ScheduledTask nextTask;
		if (requeueTask == null)
			nextTask = queueTask;
		else if (queueTask == null)
			nextTask = requeueTask;
		else if (requeueTask.getPriority() > queueTask.getPriority())
			nextTask = requeueTask;
		else
			nextTask = queueTask;

		return nextTask;
	}

	public List<ScheduledTask> findTasksByCalculationService(String name) {
		CalculationService calculationService = calculationServiceRepository.findOneByName(name);
		if(calculationService==null) 
			return Collections.emptyList();
		return taskRepository.findByCalculationService(calculationService);
	}
	
	private ScheduledTask mapTask(TaskDto dto){
		ScheduledTask task = new ScheduledTask();
		task.setTaskStatus(TaskStatus.QUEUE);
		task.setContainerId(dto.getContainerId());
		task.setUserId(dto.getUserId());
		return task;
	}

	public List<ScheduledTask> setPriority(String containerId, int priority) {
		List<ScheduledTask> tasks = taskRepository.findByContainerId(containerId);
		tasks.forEach(task -> task.setPriority(priority));
		return taskRepository.save(tasks);
	}

	public Integer getPriority(String containerId) {
		List<ScheduledTask> tasks = taskRepository.findByContainerId(containerId);

		if (tasks.isEmpty())
			return null;

		return tasks.get(0).getPriority();
	}

}
