package nl.maastro.mia.schedulingservice.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.repository.TaskRepository;
import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;

@Service
public class ContainerService {

	private final static Logger LOGGER = Logger.getLogger(ContainerService.class);

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	RestTemplate restTemplate;

	@Value("${microservice.containerservice:containerservice}")
	private String containerServiceApplication;

	public void updateCalculationStatus(ScheduledTask task, TaskStatus taskStatus){
		TaskStatus oldStatus = task.getTaskStatus();

		// The following logic makes sure that tasks are calculated a second time (but no more) when a connectionError or a calculationError occurs
		if (oldStatus == TaskStatus.RUNNING && taskStatus == TaskStatus.CONNECTIONERROR)
				taskStatus = TaskStatus.REQUEUE;
		if (oldStatus == TaskStatus.RUNNING && taskStatus == TaskStatus.CALCULATIONERROR)
				taskStatus = TaskStatus.REQUEUE;
		if (oldStatus == TaskStatus.REQUEUE && taskStatus == TaskStatus.RUNNING)
			taskStatus = TaskStatus.RERUNNING;

		LOGGER.warn("Task status for task with ID " + task.getId() + " is changed to " + taskStatus.toString());

		task.setTaskStatus(taskStatus);
		LOGGER.debug("save: " + task);
		task = taskRepository.save(task);
		updateContainerService(task);

		if(	task.getTaskStatus().equals(TaskStatus.DONE) ||
				task.getTaskStatus().equals(TaskStatus.CALCULATIONERROR) ||
				task.getTaskStatus().equals(TaskStatus.EXPORTERROR) ||
				task.getTaskStatus().equals(TaskStatus.CONNECTIONERROR) ||
				task.getTaskStatus().equals(TaskStatus.IDLE)){

			LOGGER.debug("delete: " + task);
			taskRepository.delete(task);
		}
	}

	private void updateContainerService(ScheduledTask task){
		String url = "http://"+this.containerServiceApplication+"/api/container/"+task.getContainerId()+"/status";
		restTemplate.put(url, task.getTaskStatus().toString());
	}

	public String getContainerService() {
		return containerServiceApplication;
	}

	public void setContainerService(String containerService) {
		this.containerServiceApplication = containerService;
	}
}
