package nl.maastro.mia.schedulingservice.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import nl.maastro.mia.schedulingservice.entity.CalculationService;
import nl.maastro.mia.schedulingservice.entity.ScheduledTask;
import nl.maastro.mia.schedulingservice.repository.CalculationServiceRepository;
import nl.maastro.mia.schedulingservice.repository.TaskRepository;
import nl.maastro.mia.schedulingservice.repository.enumeration.TaskStatus;

@Service
public class TaskSchedulerService {

    private final static Logger LOGGER = Logger.getLogger(TaskSchedulerService.class);

    @Autowired
    TaskService taskService;

    @Autowired
    CalculationServiceRepository calculationServiceRepository;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    EurekaClient eurekaClient;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ContainerService containerService;

    @Value("${microservice.containerservice:containerservice}")
    private String containerServiceApplication;

    @Value("${spring.application.name}")
    private String appName;

    // Check for available workers and submit tasks from queue
    public void checkQueuedTasks(){

        if(!isTaskInQueue()) return;

        for(CalculationService c : calculationServiceRepository.findAll()){

            boolean instanceFound = false;
            boolean instanceAvailable = false;

            Application application = eurekaClient.getApplication(c.getName());
            if(application==null){
                LOGGER.info("Application not registered: "+c.getName());
                continue;
            }

            ScheduledTask task = taskService.getNext(c.getName());
            if(task==null){
                LOGGER.info("No tasks for application: "+c.getName());
                calculationServiceRepository.delete(c);
                continue;
            }

            for(InstanceInfo instanceInfo : application.getInstances()){
                instanceFound = true;
                if (isInstanceAvailable(instanceInfo)) {
                    instanceAvailable = true;
                    runTask(task, instanceInfo);
                    break;
                }
            }
            LOGGER.info("Application:"+c.getName()+" instanceFound:"+instanceFound+" instanceAvailable:"+instanceAvailable);
        }
    }

    // Check that busy workers are still reachable
    public void checkRunningTasks() {
        List<ScheduledTask> runningTasks = taskRepository.findByTaskStatus(TaskStatus.RUNNING);
        runningTasks.addAll(taskRepository.findByTaskStatus(TaskStatus.RERUNNING));

        for (ScheduledTask task : runningTasks) {

            String id = task.getInstanceId();
            List<InstanceInfo> instanceList = eurekaClient.getInstancesById(id);

            if (instanceList.isEmpty()) {
                LOGGER.warn("Worker has become unreachable, ID = " + task.getInstanceId());

                containerService.updateCalculationStatus(task, TaskStatus.CONNECTIONERROR);
            }
            else {
                for (InstanceInfo instanceInfo : instanceList) {
                    if (isInstanceReachable(instanceInfo))
                        continue;
                    else {
                        LOGGER.warn("Worker has become unreachable, ID = " + instanceInfo.getId() +
                                ", host name = " + instanceInfo.getHostName() +
                                ", port = " + instanceInfo.getPort() );

                        containerService.updateCalculationStatus(task, TaskStatus.CONNECTIONERROR);
                    }
                }
            }
        }
    }

    private boolean isTaskInQueue(){
        long queueCount = taskRepository.countByTaskStatus(TaskStatus.QUEUE);
        long requeueCount = taskRepository.countByTaskStatus(TaskStatus.REQUEUE);

        long count = queueCount + requeueCount;
        LOGGER.info("tasks in queue: " + count);
        return count!=0;
    }

    private void runTask(ScheduledTask task, InstanceInfo instanceInfo){

        String container = getContainer(task);
        if(container==null){
            LOGGER.warn("Container cannot be retrieved, task will be removed: "+task);
            taskRepository.delete(task);
            return;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<String>(container, headers);
        String url = "http://"+instanceInfo.getHostName()+":"+instanceInfo.getPort()+"/api/computation";

        RestTemplate restTemplate = new RestTemplate(); //do not autowire, autowired resttemplate has loadbalancing!
        ResponseEntity<String> calculationIdResponse = restTemplate.postForEntity(url, entity, String.class);

        if(calculationIdResponse.getStatusCode().is2xxSuccessful()){
            task.setCalculationId(calculationIdResponse.getBody());
            task.setInstanceId(instanceInfo.getInstanceId());

            containerService.updateCalculationStatus(task, TaskStatus.RUNNING);
        }
        else{
            containerService.updateCalculationStatus(task, TaskStatus.IDLE);

            LOGGER.error("posting container to iap failed. Status: " +
                    calculationIdResponse.getStatusCode() + ", body: " + calculationIdResponse.getBody());
        }
    }

    private boolean isInstanceReachable(InstanceInfo instanceInfo) {
        String url = "http://"+instanceInfo.getHostName()+":"+instanceInfo.getPort()+"/api/status/available";
        try {
            RestTemplate restTemplate = new RestTemplate(); //do not autowire, autowired resttemplate has loadbalancing!
            ResponseEntity<Boolean> response = restTemplate.getForEntity(url, Boolean.class);
            if (response.getStatusCode().is2xxSuccessful())
                return true;
            else {
                LOGGER.warn("Instance is not reachable, url:" + url +
                        ", http status code:" + response.getStatusCode());
                return false;
            }
        }
        catch (RestClientException e) {
            LOGGER.warn("Instance is not reachable (RestClientException), url: " + url, e);
            return false;
        }
    }

    private boolean isInstanceAvailable(InstanceInfo instanceInfo){
        String url = "http://"+instanceInfo.getHostName()+":"+instanceInfo.getPort()+"/api/status/available";
        try{
            RestTemplate restTemplate = new RestTemplate(); //do not autowire, should not have loadbalancing
            ResponseEntity<Boolean> response = restTemplate.getForEntity(url, Boolean.class);
            if(!response.getStatusCode().is2xxSuccessful()){
                LOGGER.warn("Calculation endpoint response not successful, application:"+instanceInfo.getAppName()+" url:"+ url +" status:"+response.getStatusCode());
                return false;
            }
            return response.getBody();
        }
        catch(RestClientException e){
            LOGGER.warn("Calculation endpoint RestClientErrorException, application:"+instanceInfo.getAppName()+" url:"+ url ,e);
            return false;
        }
    }

    private String getContainer(ScheduledTask task){
        String url = "http://"+this.containerServiceApplication+"/api/container/"+task.getContainerId();
        try{
            ResponseEntity<String> container = restTemplate.getForEntity(url, String.class);
            return container.getBody();
        }
        catch(HttpClientErrorException e){
            LOGGER.error("Unable to retrieve container for task:"+task.getId()+" from url: "+url,e);
            return null;
        }
    }

}
